import { baseUrl } from '../config';

export const getPersonInformationAPI = `${baseUrl}/person`;
