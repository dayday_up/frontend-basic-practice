import $ from 'jquery';

export const ajax = (type, url, data = {}, success) => {
  $.ajax({
    url: url,
    type: type,
    data: data,
    success: success,
    error: error => {
      // eslint-disable-next-line no-console
      console.error(error);
      alert('sorry, something went wrong~');
    }
  });
};
