import $ from 'jquery';
import { ajax } from './utils';
import { getPersonInformationAPI } from '../api';

ajax('get', getPersonInformationAPI, {}, data => {
  const { name, age, description, educations } = data;
  const introduce = `my name is ${name} ${age} yo and this is my resume/cv`.toLocaleUpperCase();
  const documentTitle = `${name}'s Resume`;
  document.title = documentTitle;
  appendContentToHtml('#introduce', introduce);
  appendContentToHtml('#description', description);
  educations.forEach(education => {
    const { year, title, description } = education;
    const eductionItem = jointEductionItem(year, title, description);
    appendContentToHtml('#eduction-list', eductionItem, 'append');
  });
});

const appendContentToHtml = (element, content, type = 'html') => {
  switch (type) {
    case 'html':
      $(element).html(content);
      break;
    case 'append':
      $(element).append(content);
      break;
    default:
      return null;
  }
};

const jointEductionItem = (year, title, description) => {
  return `<li class="eduction-item">
            <h3 class="time">${year}</h3>
            <div class="detail">
              <h4 class="title">${title}</h4>
              <p class="description">${description}</p>
            </div>
          </li>`;
};
